import "bootstrap/dist/css/bootstrap.css";
import { createApp } from "vue";
import { createRouter, createWebHashHistory } from "vue-router";
import App from "./App";
import MainPage from "@/components/MainPage";
import MessagePage from "@/components/MessagePage";
import NotFound from "@/components/NotFound";

const routes = [
  { path: "/", component: MainPage },
  { path: "/message/:id", component: MessagePage },
  { path: "/not_found", component: NotFound },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

createApp(App).use(router).mount("#app");
