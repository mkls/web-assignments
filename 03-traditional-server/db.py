from psycopg2.extras import DictCursor


def get_messages(conn):
    with conn:
        with conn.cursor(cursor_factory=DictCursor) as cur:
            cur.execute('SELECT * FROM post ORDER BY claps DESC')

            for post in cur:
                yield post


def get_message(conn, msg_id: int):
    with conn:
        with conn.cursor(cursor_factory=DictCursor) as cur:
            cur.execute('SELECT * FROM post WHERE id = %s', (msg_id, ))
            res = cur.fetchone()

    return res


def inc_message_claps(conn, msg_id: int):
    with conn:
        with conn.cursor(cursor_factory=DictCursor) as cur:
            cur.execute('UPDATE post SET claps = claps + 1 WHERE id = %s',
                        (msg_id, ))
            conn.commit()


def add_message(conn, author, message):
    with conn:
        with conn.cursor() as cur:
            cur.execute('INSERT INTO post (author, msg) VALUES (%s, %s)',
                        (author, message))
