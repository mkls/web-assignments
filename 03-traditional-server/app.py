import os
import psycopg2 as pg
from db import get_message, get_messages, inc_message_claps, add_message
from flask import Flask, render_template, redirect, request

app = Flask(__name__)
connection = None


@app.get('/')
def index():
    messages = get_messages(connection)
    return render_template('index.html', messages=messages, errors=None)


@app.post('/message')
def post_message():
    form = request.form

    errors = []

    author = form.get('author', None)
    message = form.get('message', None)

    if author is None or author == "":
        errors.append("Нужно указать отправителя")
    elif len(author) > 30:
        errors.append("Имя отправителя не должно быть больше 30 символов")

    if message is None or message == "":
        errors.append("Нужно указать сообщение")
    elif len(message) > 1000:
        errors.append("Длина сообщения не должна превышать 1000 символов")

    if len(errors) == 0:
        add_message(connection, author, message)

    messages = get_messages(connection)
    return render_template('index.html', messages=messages, errors=errors)


@app.get('/message/<message_id>')
def message(message_id):
    message = get_message(connection, int(message_id))

    return render_template('message.html', message=message)


@app.post('/message/<message_id>/claps')
def clap(message_id):
    inc_message_claps(connection, int(message_id))

    return redirect(f'/message/{message_id}')


if __name__ == '__main__':
    host = os.getenv('HOST', '0.0.0.0')
    port = os.getenv('PORT', 5000)

    db_host = os.getenv('DB_HOST', 'localhost')
    db_port = os.getenv('DB_PORT', 5432)
    db_user = os.getenv('DB_USER', 'postgres')
    db_password = os.getenv('DB_PASSWORD', None)
    db_name = os.getenv('DB_NAME', 'postgres')

    connection = pg.connect(host=db_host,
                            port=db_port,
                            dbname=db_name,
                            user=db_user,
                            password=db_password)

    app.run(host=host, port=port)

    connection.close()
