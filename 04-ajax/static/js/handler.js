function createMessageLi(id, author, message, claps) {
    let li = document.createElement('li');
    li.classList.add('mb-3');

    let article = document.createElement('article');
    article.classList.add('card')
    li.appendChild(article);

    let div = document.createElement('div');
    div.classList.add('card-body');
    article.appendChild(div);

    let header = document.createElement('header');
    header.classList.add('card-title');
    header.classList.add('d-flex');
    div.appendChild(header);

    let authorDiv = document.createElement('div');
    authorDiv.classList.add('text-muted');
    authorDiv.textContent = author;
    header.appendChild(authorDiv);

    let link = document.createElement('a');
    link.classList.add('card-link');
    link.classList.add('ms-auto');
    link.textContent = "Открыть ↗️";
    link.href = `/message/${id}`;
    header.appendChild(link);

    let msgText = document.createElement('div');
    msgText.classList.add('card-text');
    msgText.textContent = message;
    div.appendChild(msgText);

    let clapsDiv = document.createElement('div');
    clapsDiv.classList.add('d-flex');
    div.appendChild(clapsDiv);

    let clapsForm = document.createElement('form');
    clapsForm.classList.add('ms-auto');
    clapsForm.id = 'claps-form';
    clapsForm.method = "post";
    clapsDiv.appendChild(clapsForm);

    let clapsButton = document.createElement('button');
    clapsButton.classList.add('btn');
    clapsForm.textContent = '👏';
    clapsForm.appendChild(clapsButton);
    clapsForm.addEventListener('submit', event => {
        event.preventDefault();
        clap(id);
    });

    let clapsSpan = document.createElement('span');
    clapsSpan.textContent = `${claps}`;
    clapsButton.appendChild(clapsSpan);

    return li;
}

function clap(id) {
    let req = new XMLHttpRequest();
    let button = document.getElementById('submit-btn');

    req.open("POST", `http://localhost:5050/message/${id}/claps`, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.responseType = 'json';

    button.disabled = true;

    req.onload = function() {
        if (req.readyState === XMLHttpRequest.DONE) {
            if (req.status === 200) {
                refreshContents();
            } else {
                showError(req.response, 'alert-danger');
            }
        }

        button.disabled = false;
    }

    req.send();
}

function refreshContents() {
    let list = document.getElementById("message-list");

    // Reset list items
    list.textContent = '';

    let xhr = new XMLHttpRequest();

    xhr.open("GET", "http://localhost:5050/messages", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.responseType = 'json';

    xhr.onload = function() {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            for (var i = 0; i < xhr.response.length; i++) {
                let author = xhr.response[i].author;
                let message = xhr.response[i].msg;
                let id = xhr.response[i].id;
                let claps = xhr.response[i].claps;

                let li = createMessageLi(id, author, message, claps);
                list.appendChild(li);
            }
        }
    }

    xhr.send();
}

function showError(errorMessage, errorClass) {
    let msgAlert = document.getElementById('msg-alert');

    let alertDiv = document.getElementById('alert-div');
    if (alertDiv == null) {
        alertDiv = document.createElement('div');
        alertDiv.id = 'alert-div';
        msgAlert.insertBefore(alertDiv, msgAlert.firstChild);
    }
    alertDiv.textContent = '';
    alertDiv.classList.add('mb-3');

    let alertDivDanger = document.createElement('div');
    alertDivDanger.classList.add('alert');
    alertDivDanger.classList.add(errorClass);
    alertDivDanger.textContent = errorMessage;
    alertDiv.appendChild(alertDivDanger);
}

function postMessage() {
    let formData = new FormData(form);

    let author = formData.get('author');
    let message = formData.get('message');

    console.log(author.length);
    console.log(message.length);

    if (author.length == 0) {
        showError('Укажите автора', 'alert-danger');
        return;
    } else if (author.length > 30) {
        showError('Имя автора не должно быть больше 30 символов', 'alert-danger');
        return;
    }

    if (message.length == 0) {
        showError('Укажите сообщение', 'alert-danger');
        return;
    } else if (message.length > 1000) {
        showError('Текст сообщения не должен превышать 1000 символов', 'alert-danger');
        return;
    }

    showError('Отправляем запрос…', 'alert-warning');

    let req = new XMLHttpRequest();
    req.open("POST", "http://localhost:5050/messages", true);
    req.setRequestHeader("Content-Type", "application/json");
    req.responseType = 'json';

    req.onload = function() {
        if (req.readyState === XMLHttpRequest.DONE) {
            if (req.status === 201) {
                showError('Сообщение добавлено', 'alert-success')
                refreshContents();
            } else {
                showError(req.response, 'alert-danger');
            }
        }
    }

    req.send(JSON.stringify({"author": author, "message": message}));
}

document.addEventListener('DOMContentLoaded', refreshContents, false);

let form = document.getElementById("msg-form");
form.addEventListener('submit', event => {
    event.preventDefault();
    postMessage();
})

