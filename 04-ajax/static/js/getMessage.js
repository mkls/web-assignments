function setClaps(clapsCount) {
    let clapsSpan = document.getElementById('claps-span');
    clapsSpan.textContent = `${clapsCount}`;
}

function clap(id) {
    let req = new XMLHttpRequest();
    let button = document.getElementById('clap-btn');
    button.disabled = true;

    req.open("POST", `http://localhost:5050/message/${id}/claps`, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.responseType = 'json';

    req.onload = function() {
        if (req.readyState === XMLHttpRequest.DONE) {
            if (req.status === 200) {
                setClaps(req.response.claps);
            } else {
                showError(req.response, 'alert-danger');
            }
            button.disabled = false;
        }
    }

    req.send();
}

function showMessage(id, author, message, claps) {
    let card = document.getElementById('msg-card-body');
    card.textContent = '';

    let header = document.createElement('header');
    header.classList.add('card-title');
    header.classList.add('d-flex');
    card.appendChild(header);

    let authorDiv = document.createElement('div');
    authorDiv.classList.add('text-muted');
    authorDiv.textContent = author;
    header.appendChild(authorDiv);

    let msgText = document.createElement('div');
    msgText.classList.add('card-text');
    msgText.textContent = message;
    card.appendChild(msgText);

    let clapsDiv = document.createElement('div');
    clapsDiv.classList.add('d-flex');
    card.appendChild(clapsDiv);

    let clapsForm = document.createElement('form');
    clapsForm.classList.add('ms-auto');
    clapsForm.method = "post";
    clapsDiv.appendChild(clapsForm);
    clapsForm.addEventListener('submit', event => {
        event.preventDefault();
        clap(id);
    });

    let clapsButton = document.createElement('button');
    clapsButton.classList.add('btn');
    clapsButton.id = 'clap-btn';
    clapsForm.textContent = '👏';
    clapsForm.appendChild(clapsButton);

    let clapsSpan = document.createElement('span');
    clapsSpan.id = 'claps-span';
    clapsSpan.textContent = `${claps}`;
    clapsButton.appendChild(clapsSpan);
}

function getMessage() {
    const url = new URL(window.location.href);
    const msgId = url.pathname.split('/').pop();

    let xhr = new XMLHttpRequest();
    xhr.open("GET", `http://localhost:5050/message/${msgId}`, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.responseType = 'json';
    xhr.onload = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status == 200) {
                let author = xhr.response.author;
                let message = xhr.response.msg;
                let id = xhr.response.id;
                let claps = xhr.response.claps;

                showMessage(id, author, message, claps);
            } else if (xhr.status == 404) {
                window.location.href = '/not_found';
            }
        }
    };

    xhr.send();
}

document.addEventListener('DOMContentLoaded', getMessage, false);
