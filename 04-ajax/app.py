import os
from flask import Flask, render_template
from werkzeug.exceptions import NotFound

app = Flask(__name__)
connection = None


@app.get('/')
def index():
    return render_template('index.html')


@app.get('/message/<message_id>')
def message(message_id):
    return render_template('message.html')


@app.get('/not_found')
@app.errorhandler(NotFound)
def not_found(*args):
    return render_template('not_found.html')


if __name__ == '__main__':
    host = os.getenv('HOST', '0.0.0.0')
    port = os.getenv('PORT', 5000)

    app.run(host=host, port=port)
