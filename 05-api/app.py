import os
import psycopg2 as pg
import logging
from db import get_message, get_messages, inc_message_claps, add_message
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
CORS(app)

connection = None


@app.get('/messages')
def messages():
    messages = get_messages(connection)
    return jsonify(list(map(dict, messages)))


@app.post('/messages')
def post_message():
    body = request.json
    errors = []

    if body is None:
        return "Is content-type application/json?", 400

    author = body.get('author', None)
    message = body.get('message', None)

    if author is None or author == "":
        return "Нужно указать отправителя", 422
    if len(author) > 30:
        return "Имя отправителя не должно быть больше 30 символов", 422

    if message is None or message == "":
        return "Нужно указать сообщение", 422
    if len(message) > 1000:
        return "Длина сообщения не должна превышать 1000 символов", 422

    if len(errors) == 0:
        msg = add_message(connection, author, message)
        logging.warning(msg)

    return msg, 201


@app.get('/message/<message_id>')
def message(message_id):
    message = get_message(connection, int(message_id))

    if message is None:
        return 'Not found', 404

    return dict(message)


@app.post('/message/<message_id>/claps')
def clap(message_id):
    if get_message(connection, int(message_id)) is not None:
        claps = inc_message_claps(connection, int(message_id))
        return {'claps': claps['claps']}  # ???
    else:
        return 'Not found', 404


if __name__ == '__main__':
    host = os.getenv('HOST', '0.0.0.0')
    port = os.getenv('PORT', 5000)

    db_host = os.getenv('DB_HOST', 'localhost')
    db_port = os.getenv('DB_PORT', 5432)
    db_user = os.getenv('DB_USER', 'postgres')
    db_password = os.getenv('DB_PASSWORD', None)
    db_name = os.getenv('DB_NAME', 'postgres')

    connection = pg.connect(host=db_host,
                            port=db_port,
                            dbname=db_name,
                            user=db_user,
                            password=db_password)

    app.run(host=host, port=port)

    connection.close()
